FROM node:latest

RUN npm install

WORKDIR /app

CMD ['node', 'index.js']
