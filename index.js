const io = require('socket.io')(8080);

io.on('connection', function(socket) {
  console.log('A user has been connected.');
  socket.emit('message:server', {whom: 'server', message: 'You are connected now!'});
  socket.broadcast.emit('message:server', {whom: 'server', message: 'An user has been connected now! :)'})

  socket.on('message:client', function(message) {
    socket.broadcast.emit('message:server', message);
  });

  socket.on('disconnect', function() {
    console.log('A user has been disconnected.')
    socket.broadcast.emit('message:server', {whom: 'server', message: 'An user has been disconnected :(' });
  });
});
